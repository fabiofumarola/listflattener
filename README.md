# README #


### Set up the docker having the web service ###
Build the docker image

```
#!bash

cd ./docker
chmod +x build.sh
./build.sh
```

Run the image:

```
#!bash
run -dt --name listflattener -p 4568:4568 kdde/listflattener

```