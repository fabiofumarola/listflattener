package it.dtk.listflattener.webServer;

import java.util.ArrayList;

/**
 * Created by fabiana on 11/17/15.
 */
public class LogicalList {
    public String url_website;
    public String url_dataRecord;
    public ArrayList<ExtensionList> extensionLists = new ArrayList<>();


    public LogicalList(String url_website, String url_dataRecord){
        this.url_website = url_website;
        this.url_dataRecord = url_dataRecord;
    }
    public void addExtensionList(int sizeList, ArrayList<String> extensionLists, double similarity, String inLink){
        ExtensionList e = new ExtensionList(this.extensionLists.size()+1, sizeList, extensionLists, similarity, inLink);
        this.extensionLists.add(e);
    }
    public class ExtensionList{
        public int id;
        public int sizeList;
        public double similarity;
        public String inLink;
        public ArrayList<String> extensionList;
        public ExtensionList (int id, int sizeList, ArrayList<String> extensionList, double similarity, String inLink){
            this.id = id;
            this.sizeList = sizeList;
            this.extensionList = extensionList;
            this.similarity = similarity;
            this.inLink = inLink;
        }
    }
}
