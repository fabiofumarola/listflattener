package it.dtk.listflattener.webServer;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.dtk.listflattener.ListExtractor;
import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebList;
import it.dtk.listflattener.model.WebPage;
import it.dtk.traverser.TraverserFactory;
import it.dtk.utils.StringNormalizer;

import java.util.ArrayList;
import java.util.List;

import static spark.Spark.*;
/**
 * Created by fabiana on 11/17/15.
 */
public class ListFlattener {

    public static void main(String[] args) {
        port(4568);

        /**
         * Utilizzo del framework SparkJava, permette di effetuare una richiesta
         * get. Restituisce i dataitem allineati e stampati in formato json.
         * @Parma :url = http://localhost:4567/datarecordextractor/http://nomesito
         */
        get("/listFlattener",(request, response) -> {

            response.type("application/json");
            String urlWebsite = request.queryParams("webpage");
            String urlDatarecord = request.queryParams("datarecord").toLowerCase();
            System.out.println("website " + urlWebsite + "\ndatarecord " + urlDatarecord);
            String urlService = "http://193.204.187.132:15000/traverseAsync?url=";
            TraverserFactory.vpnUrl = urlService;
            double tagFactor = 0.4d;
            double structSim = 0.6d;

            WebPage page = it.dtk.listflattener.ListFlattener.extract(urlWebsite, urlDatarecord, tagFactor, structSim, true);
            LogicalList result = new LogicalList(urlWebsite, urlDatarecord);
            for (WebList extensionList : page.getExtensionLists()) {
                updateLogicalList(extensionList,result);
            }

            ObjectMapper mapper = new ObjectMapper();
            //return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
            return mapper.writeValueAsString(result);
        });
        get("/listFlattenerTest", (req, res) -> "Hello World, Server online");

    }



    private static void updateLogicalList(WebList list, LogicalList result) {



        for (WebList extensionList : list.getExtensionLists().values()) {
            int j = 1;
            ArrayList<String> stringList = new ArrayList<String>();
            for (WebElement we : extensionList.getWebElements()) {
                stringList.add(j + " \t" + we.getHtml());
                j++;
            }
            Double similarity = 1 - (extensionList.getSumSimilarity()/extensionList.size());
            result.addExtensionList(extensionList.size(),stringList, similarity, extensionList.getWebPage().getUrl());
        }

    }
}
