package it.dtk.listflattener.filter;

import it.dtk.listflattener.model.Orientation;
import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebList;

import java.util.List;
import java.util.Map;

public class HorizontalListFilter extends ListFilter {

	public HorizontalListFilter(WebElement element) {
		super(element);
	}

	@Override
	public void filter() {
		Map<Integer, List<WebElement>> horList = element
				.filterChildrenAlignedY();

		for (List<WebElement> l : horList.values()) {
			if (l.size() > 1) {
				WebList webList = new WebList(element, l,
						Orientation.Horizontal);
				aligned.add(webList);
			} else {
				notAligned.put(l.get(0).hashCode(), l.get(0));
			}
		}
	}

}
