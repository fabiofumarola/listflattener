package it.dtk.listflattener.filter;

import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ListFilter {

	protected WebElement element;

	protected List<WebList> aligned;

    //Map[position,Element]
	protected Map<Integer, WebElement> notAligned;

	public ListFilter(WebElement element) {
		this.element = element;
		this.aligned = new ArrayList<>();
		this.notAligned = new HashMap<>();
	}

	public abstract void filter();

	public List<WebList> getAligned() {
		return aligned;
	}

	public void setAligned(List<WebList> aligned) {
		this.aligned = aligned;
	}

	public Map<Integer, WebElement> getNotAligned() {
		return notAligned;
	}

	public void setNotAligned(Map<Integer, WebElement> notAligned) {
		this.notAligned = notAligned;
	}

}
