package it.dtk.listflattener.filter;

import it.dtk.listflattener.model.Orientation;
import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MergeFilter extends ListFilter {

    public MergeFilter(WebElement element) {
        super(element);
    }

    /**
     * @param lists
     */
    public void filter(List<WebList> lists, double tagFactor) {

        Map<Integer, WebList> output = new HashMap<>();

        for (WebList l : lists) {

            // get the hash of the web element containing the list
            Integer key = l.hashCode();

            if (!output.containsKey(key)) {
                output.put(key, l);
            } else {
                WebList webList = output.get(key);
                Double d = webList.partialStructuralDistance(l);
                if (d <= tagFactor) {
                    webList.addAll(l);
                    webList.incSumSimilarity(l.getSumSimilarity());
                }
            }

        }

        for (WebList l : output.values()) {
            Map<Integer, WebElement> mapAligned = new HashMap<>();

            Orientation orientation = l.getOrientation();
            double sumSim = l.getSumSimilarity();

            for (WebElement e : l.getWebElements()) {
                // get the webelement hash
                Integer key = e.hashCode();
                // remove the duplicates
                // if the map contains the same element do not add it to the
                // merged list
                if (!mapAligned.containsKey(key))
                    mapAligned.put(key, e);
                else
                    orientation = Orientation.Merged;
            }

            aligned.add(new WebList(l.getParent(), new ArrayList<>(mapAligned
                    .values()), orientation, sumSim));
        }
    }

    @Override
    public void filter() {
        throw new RuntimeException("this method must be not implemented");
    }

}
