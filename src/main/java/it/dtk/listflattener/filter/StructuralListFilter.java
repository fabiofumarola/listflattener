package it.dtk.listflattener.filter;

import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebList;

/**
 * @author class to find all the list of web elements structural aligned
 * 
 */
public class StructuralListFilter extends ListFilter {

	public StructuralListFilter(WebElement element) {
		super(element);
	}

	public void filter(WebList inputList, double tagFactor) {

		boolean[] checked = new boolean[inputList.size()];

		WebElement a, b;
		for (int i = 0; i < inputList.size(); i++) {
			if (!checked[i]) {
				a = inputList.get(i);

				WebList list = new WebList(inputList.getParent(),
						inputList.getOrientation());
				list.add(a);
				checked[i] = true;

				for (int j = i + 1; j < inputList.size(); j++) {
					if (!checked[j]) {
						b = inputList.get(j);
						double sim = a.structuralDistance(b);
						if (sim <= tagFactor) {
							list.add(b);
							checked[j] = true;
							list.incSumSimilarity(sim);
						}
					}
				}

				if (list.size() > 1) {
					aligned.add(list);
				} else {
					for (WebElement e : list.getWebElements()) {
						Integer key = e.hashCode();
						if (!notAligned.containsKey(key))
							notAligned.put(key, e);
					}
				}
			}
		}
	}

	@Override
	public void filter() {
		throw new RuntimeException("this method must be not implemented");
	}

}
