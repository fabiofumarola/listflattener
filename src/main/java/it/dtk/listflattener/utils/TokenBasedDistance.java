package it.dtk.listflattener.utils;

import it.dtk.utils.Levenshtein;

import java.util.HashMap;
import java.util.Map;

public class TokenBasedDistance {

	public static double normalizedDistance(String a, String b, String separator) {
		int asciiValue = 33;

		String aTokens[] = a.split(separator);
		String bTokens[] = b.split(separator);

		String aTransformed = "";
		String bTransformed = "";

		Map<String, Character> tags = new HashMap<>();

        for (String aToken : aTokens) {
            if (!tags.containsKey(aToken)) {
                tags.put(aToken, (char) asciiValue);
                asciiValue++;
            }

            aTransformed += tags.get(aToken);
        }

        for (String bToken : bTokens) {
            if (!tags.containsKey(bToken)) {
                tags.put(bToken, (char) asciiValue);
                asciiValue++;
            }

            bTransformed += tags.get(bToken);
        }

		return Levenshtein.normalizedDistance(aTransformed, bTransformed);
	}

	public static double similiarity(String a, String b, String separator) {
		int asciiValue = 33;

		String aTokens[] = a.split(separator);
		String bTokens[] = b.split(separator);

		String aTransformed = "";
		String bTransformed = "";

		Map<String, Character> tags = new HashMap<>();

        for (String aToken : aTokens) {
            if (!tags.containsKey(aToken)) {
                tags.put(aToken, (char) asciiValue);
                asciiValue++;
            }

            aTransformed += tags.get(aToken);
        }

        for (String bToken : bTokens) {
            if (!tags.containsKey(bToken)) {
                tags.put(bToken, (char) asciiValue);
                asciiValue++;
            }

            bTransformed += tags.get(bToken);
        }

		return Levenshtein.similiarity(aTransformed, bTransformed);
	}

}
