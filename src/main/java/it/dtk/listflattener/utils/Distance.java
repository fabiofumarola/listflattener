package it.dtk.listflattener.utils;

import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebList;

import com.infomatiq.jsi.Rectangle;

public class Distance {

	/**
	 * @param a
	 * @param b
	 * @return
	 */
	public static double eucledianDistance(WebList a, WebList b) {
		Rectangle aRectangle = Utils.getRectangle(a.getPosition());
		Rectangle bRectangle = Utils.getRectangle(b.getPosition());

		return aRectangle.distance(bRectangle);
	}

	/**
	 * @param a
	 * @param b
	 * @return
	 */
	public static double tagPathDistance(WebList a, WebList b) {
		String aTagPath = getTagPath(a.getParent());
		String bTagPath = getTagPath(b.getParent());

		return TokenBasedDistance.normalizedDistance(aTagPath, bTagPath, "/");
	}

	private static String getTagPath(WebElement e) {
		return e.getParentPath() + "/" + e.getNodeTag();
	}

}
