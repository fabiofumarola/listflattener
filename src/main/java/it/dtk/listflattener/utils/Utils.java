package it.dtk.listflattener.utils;

import com.infomatiq.jsi.Rectangle;
import it.dtk.listflattener.model.Position;

public class Utils {

	public static int countOccurence(String str, String pattern) {
		int count = 0;
		int pos = -1;

		while ((pos = str.indexOf(pattern, pos + 1)) != -1) {
			count++;
		}

		return count;
	}

	public static Rectangle getRectangle(Position pos) {
		Rectangle r = new Rectangle();

		r.maxX = pos.getRight();
		r.minX = pos.getLeft();
		r.maxY = pos.getBottom();
		r.minY = pos.getTop();

		return r;
	}

}
