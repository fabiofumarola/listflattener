package it.dtk.listflattener;

import it.dtk.listflattener.filter.MergeFilter;
import it.dtk.listflattener.model.Response;
import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebList;
import it.dtk.listflattener.model.WebPage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListExtractor {

    /**
     * @param url
     * @param tagFactor
     * @param timeStamp
     * @param onlyListWithUrls
     *                         <ul>
     *                         <li>if true WebPageTraverser command is ""WebPageTraverser &lturl&gt"",
     *                         </li>
     *                         <li>""xvfb-run -a WebPageTraverser  &lturl&gt"" otherwise.</li>
     *                         </ul>
     * @return
     * @throws IOException
     */
    public static WebPage extract(String url, double tagFactor, long timeStamp,
                                  boolean onlyListWithUrls) throws Exception {

        // traversing html
        WebPage page = new WebPage(url, timeStamp);
        double startTraversing = System.currentTimeMillis();

        Response response = Response.traverse(url);

        if (response.getError() != null)
            throw new Exception(response.getError().getMessage());

        page.setRedirectUrl(response.getRedirectUrl());
        page.setTraversedHtml(response.getWebElement());

        double timeTraversing = (System.currentTimeMillis() - startTraversing) / 1000;
        System.out.println("Json + Pojo = " + timeTraversing);

        double startExtracting = System.currentTimeMillis();
        List<WebList> lists = findLists(page.getTraversedHtml(), tagFactor);

        MergeFilter mergeFilter = new MergeFilter(null);
        mergeFilter.filter(lists, tagFactor);
        lists = mergeFilter.getAligned();

        double timeExtractingLists = (System.currentTimeMillis() - startExtracting) / 1000;

        // sort lists elements
        for (WebList l : lists) {
            l.sortElements();
        }

        page.setWebLists(lists);

        System.out.println("Computation = " + timeExtractingLists);

        if (onlyListWithUrls) {
            page.removeListWithoutUrls();
        }

        return page;
    }

    /**
     * @param webE
     * @param tagFactor
     * @return
     */
    private static List<WebList> findLists(WebElement webE, double tagFactor) {
        List<WebList> lists = new ArrayList<>();
        ListsFinder finder = new ListsFinder(webE, tagFactor);

        lists.addAll(finder.getAligned());

        for (WebElement e : finder.getNotAligned()) {
            lists.addAll(findLists(e, tagFactor));
        }

        return lists;
    }

    public static void close(){
        Response.traverser.close();
    }

}
