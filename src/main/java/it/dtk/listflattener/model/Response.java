package it.dtk.listflattener.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.dtk.traverser.IPageTraverser;
import it.dtk.traverser.TraverserFactory;

import java.io.Serializable;
import java.util.Optional;

/**
 * Created by fabiofumarola on 27/10/14.
 */
public class Response implements Serializable {

    private String url;
    private String redirectUrl;
    private String time;
    private WebElement webElement;
    private Error error;

    public Response(String url, String redirectUrl, String time, WebElement webElement, Error error) {
        this.url = url;
        this.redirectUrl = redirectUrl;
        this.time = time;
        this.webElement = webElement;
        this.error = error;
    }

    public Response() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public WebElement getWebElement() {
        return webElement;
    }

    public void setWebElement(WebElement webElement) {
        this.webElement = webElement;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    /**
     * TraverserFactory.getTraverser(Optional.of("http://193.204.187.132:15000/traverse?url="));
     */
    public static IPageTraverser traverser = TraverserFactory.getTraverser(Optional.empty());

    public static Response traverse(String url) throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        String output = traverser.traverse(url);
        return mapper.readValue(output, Response.class);
    }
}
