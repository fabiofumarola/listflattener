package it.dtk.listflattener.model;

import java.io.Serializable;

public class Size implements Serializable{

	private int height, width;
	
	public Size() {
	}

	public Size(int height, int width) {
		super();
		this.height = height;
		this.width = width;
	}

	public int area() {
		return this.height * this.width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	@Override
	public String toString() {
		return "Size [height=" + height + ", width=" + width + "]";
	}

}
