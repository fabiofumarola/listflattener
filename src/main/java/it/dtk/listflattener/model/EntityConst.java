package it.dtk.listflattener.model;

import it.dtk.utils.LFHash;
import it.dtk.utils.SHA256;

/*
 * Author: Michele Damiano Torelli
 * Project: ListFlattener
 * Date: 15/07/13
 * Time: 16:18
 */
final class EntityConst {
    protected static final String DEFAULT_SEPARATOR = "-";
    protected static final int MAX_ID_LENGTH = 10;
    protected static LFHash LF_HASH = SHA256.getInstance();
}
