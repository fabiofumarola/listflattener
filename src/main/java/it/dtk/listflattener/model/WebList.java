package it.dtk.listflattener.model;

import it.dtk.listflattener.utils.TokenBasedDistance;
import it.dtk.utils.StringNormalizer;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class WebList implements Serializable {
    protected WebElement parent;
    protected List<WebElement> elements;
    protected Orientation orientation;
    protected double sumSimilarity;

    private Position position;
    private WebPage webPage;
    private Integer id;
    private Integer nextWebElementId = 0;
    private Map<WebElement, WebList> extensionLists;

    public WebList(WebElement parent) {
        this.parent = parent;
        this.webPage = parent.getWebPage();
        this.elements = new ArrayList<>();
        this.orientation = Orientation.Undefined;
        this.extensionLists = new HashMap<>();
    }

    public WebList(WebElement parent, Orientation orientation) {
        this(parent);
        this.orientation = orientation;
    }

    public WebList(WebElement parent, List<WebElement> l,
                   Orientation orientation) {
        this(parent, orientation);
        this.elements = l;
        setWebList();
    }

    public WebList(WebElement parent, List<WebElement> l,
                   Orientation orientation, double sumSim) {
        this(parent, l, orientation);
        sumSimilarity = sumSim;
    }

    public boolean isExtendedWebList() {
        return !this.extensionLists.isEmpty();
    }

    public int getId() {
        if (this.id == null) {
            this.id = parent.getWebPage().getNextWebListId();
        }

        return this.id;
    }

    public String getKey() {
        return webPage.getHash()
                + EntityConst.DEFAULT_SEPARATOR
                + StringNormalizer.normalize(String.valueOf(id), EntityConst.MAX_ID_LENGTH);
    }

    public int getNextWebElementId() {
        return nextWebElementId++;
    }

    private void setWebList() {
        for (WebElement we : elements) {
            we.setWebList(this);
        }
    }

    /**
     * @param b
     * @return the partial distance considering equal number of elements
     */
    public double partialStructuralDistance(WebList b) {
        int min = Math.min(this.elements.size(), b.elements.size());

        String thisBfsTree = "";
        String bBfsTree = "";

        for (int i = 0; i < min; i++) {
            thisBfsTree += elements.get(i).getBFSTree() + "/";
            bBfsTree += b.elements.get(i).getBFSTree() + "/";
        }
        thisBfsTree = thisBfsTree.substring(0, thisBfsTree.length() - 1);
        bBfsTree = bBfsTree.substring(0, bBfsTree.length() - 1);

        return TokenBasedDistance.normalizedDistance(thisBfsTree, bBfsTree, "/");
    }

    public List<WebElement> getWebElements() {
        return elements;
    }

    public void setWebElements(List<WebElement> webElements) {
        this.elements = webElements;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public WebElement getParent() {
        return parent;
    }

    public void setParent(WebElement parent) {
        this.parent = parent;
    }

    public WebPage getWebPage() {
        return webPage;
    }

    public void setWebPage(WebPage webPage) {
        this.webPage = webPage;
    }

    public int size() {
        return elements.size();
    }

    public WebElement get(int index) {
        return elements.get(index);
    }

    public void add(WebElement e) {
        elements.add(e);
    }

    @Override
    public int hashCode() {
        return parent.hashCode();
    }

    public void print() {
        System.out.println("List of length " + size() + "; "
                + orientation + "; avg similarity: " + sumSimilarity / size() + "From url: "+ this.getWebPage().getUrl());

        int i = 1;
        for (WebElement e : elements) {
            System.out.println(i + " \t" + e.getHtml());
            System.out.println("--------------------");
            i++;
        }

        if (isExtendedWebList()) {
            System.out.println("\nIs an extended WebList:");

            for (WebList extensionList : getExtensionLists().values()) {
                System.out.println("List from url: "+ extensionList.getWebPage().getUrl()+ " List of length: " + extensionList.size() + "; "
                        + extensionList.orientation + "; avg similarity: " + (1-(extensionList.sumSimilarity /extensionList.size()))+ "; similarity: "+extensionList.sumSimilarity /extensionList.size()   );
                int j = 1;
                for (WebElement we : extensionList.getWebElements()) {
                    System.out.println(j + " \t" + we.getHtml());
                    System.out.println("--------------------");
                    j++;
                }
            }
        }
    }

    public void addAll(WebList l) {
        elements.addAll(l.elements);
    }

    public double getSumSimilarity() {
        return sumSimilarity;
    }

    public void setSumSimilarity(double sumSimilarity) {
        this.sumSimilarity = sumSimilarity;
    }

    public void incSumSimilarity(double value) {
        sumSimilarity += value;
    }

    public Set<String> getUrls() {
        Set<String> urls = new HashSet<>();

        for (WebElement e : elements) {
            urls.addAll(e.getUrls());
        }
        return urls.stream().filter(u -> !u.contains("@")).collect(Collectors.toSet());
    }

    public long getTimeStamp() {
        return this.webPage.getTimeStamp();
    }

    public Map<WebElement, WebList> getExtensionLists() {
        return this.extensionLists;
    }

    @Override
    public String toString() {
        String str = "";

        str += "List of length " + size() + "; " + orientation + "\n";
        str += "avg similarity: " + sumSimilarity / size() + "\n";

        if (elements.size() > 0) {
            str += "\t" + elements.get(0) + "\n";
            str += "--------------------" + "\n";
        }

        return str;
    }

    public Position getPosition() {
        if (position != null)
            return position;

        int minTop = Integer.MAX_VALUE, maxBottom = Integer.MIN_VALUE, minLeft = Integer.MAX_VALUE, maxRight = Integer.MIN_VALUE;

        for (WebElement e : elements) {

            if (e.getSize().area() <= 0)
                continue;

            Position p = e.getPosition();
            if (p.getTop() < minTop)
                minTop = p.getTop();
            if (p.getBottom() > maxBottom)
                maxBottom = p.getBottom();
            if (p.getLeft() < minLeft)
                minLeft = p.getLeft();
            if (p.getRight() > maxRight)
                maxRight = p.getRight();
        }
        position = new Position(maxBottom, minLeft, maxRight, minTop);

        return position;

    }

    public PositionType relativePosition(WebList b) {
        return getPosition().physicalPosition(b.getPosition());
    }

    public void sortElements() {
        Collections.sort(elements);
    }

}
