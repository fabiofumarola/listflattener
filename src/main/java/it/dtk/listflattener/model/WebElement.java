package it.dtk.listflattener.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import it.dtk.listflattener.utils.TokenBasedDistance;

import java.io.Serializable;
import java.util.*;

public class WebElement implements Comparable<WebElement>, Serializable {

    private Integer id;
    private Map<String, String> attributes;
    private List<WebElement> children;
    private String nodeTag;
    private String parentPath;
    private String parentDomCSSPath;
    private String parentCSSPath;
    private Position position;
    private Size size;
    private String text;
    private String bfstree;
    private String idBfsTree;
    private Map<String, String> urls;
    private int numNodes;
    private Boolean displayed;
    private WebPage webPage;
    private WebList webList;

    public WebElement() {
        this.attributes = new HashMap<>();
        this.children = new LinkedList<>();
        this.urls = new HashMap<>();
    }

    private boolean displayed() {
        if (displayed == null)
            displayed = !nodeTag.contains("script");

        return displayed;
    }

    /**
     * @return a map of left position and list o web elements aligned to the
     * position
     */
    public Map<Integer, List<WebElement>> filterChildrenAlignedX() {
        Map<Integer, List<WebElement>> aligned = new HashMap<>();

        for (WebElement child : children) {
            if (!child.displayed())
                continue;

            Integer x = child.getPosition().getLeft();

            if (!aligned.containsKey(x))
                aligned.put(x, new ArrayList<WebElement>());

            aligned.get(x).add(child);
        }

        return aligned;
    }

    /**
     * @return the children aligned to Y
     */
    public Map<Integer, List<WebElement>> filterChildrenAlignedY() {
        Map<Integer, List<WebElement>> aligned = new HashMap<>();

        for (WebElement child : children) {
            if (!child.displayed())
                continue;

            Integer y = child.getPosition().getTop();

            if (!aligned.containsKey(y))
                aligned.put(y, new ArrayList<WebElement>());

            aligned.get(y).add(child);
        }

        return aligned;
    }

    @JsonIgnore
    public String getBFSTree() {
        if (bfstree == null) {
            Queue<WebElement> queue = new LinkedList<>();
            queue.add(this);

            String bfs = nodeTag;
            numNodes = 1;
            String idBfs = id.toString() + " ";

            if (nodeTag.equals("a")) {
                String url = attributes.get("href");

                if (url != null) {
                    urls.put(url, this.text);
                }
            }

            while (!queue.isEmpty()) {
                WebElement elem = queue.remove();
                for (WebElement child : elem.children) {
                    if (!child.displayed())
                        continue;

                    bfs += "/" + child.nodeTag;
                    idBfs += child.id + " ";
                    queue.add(child);
                    numNodes++;

                    if (child.nodeTag.equals("a")) {
                        String url = child.attributes.get("href");
                        if (url != null) {
                            if (!url.matches("(javascript:|#)(.*)")) {
                                urls.put(url, child.text);
                            }
                        }

                    }
                }
            }

            bfstree = bfs;
            idBfsTree = idBfs.trim();
        }

        return bfstree;
    }

    public double structuralDistance(WebElement b) {
        if ((this.numNodes > 40) || (b.numNodes > 40))
            return Double.MAX_VALUE;

        return TokenBasedDistance.normalizedDistance(this.getBFSTree(),
                b.getBFSTree(), "/");
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public List<WebElement> getChildren() {
        return children;
    }

    public void setChildren(List<WebElement> children) {
        this.children = children;
    }

    public String getNodeTag() {
        return nodeTag;
    }

    public void setNodeTag(String nodeTag) {
        this.nodeTag = nodeTag;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }

    public String getParentCSSPath() {
        return parentCSSPath;
    }

    public void setParentCSSPath(String parentCSSPath) {
        this.parentCSSPath = parentCSSPath;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public String getParentDomCSSPath() {
        return parentDomCSSPath;
    }

    public void setParentDomCSSPath(String parentDomCSSPath) {
        this.parentDomCSSPath = parentDomCSSPath;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @JsonIgnore
    public int getNumNodes() {
        if (numNodes == 0) {
            getBFSTree();
        }

        return numNodes;
    }

    @JsonIgnore
    public void setNumNodes(int numNodes) {
        this.numNodes = numNodes;
    }

    @JsonIgnore
    public long getTimeStamp() {
        return this.webPage.getTimeStamp();
    }

    @JsonIgnore
    public WebList getWebList() {
        return this.webList;
    }

    @JsonIgnore
    public void setWebList(WebList webList) {
        this.webList = webList;

        for (WebElement we : children) {
            we.setWebList(webList);
        }
    }

    @Override
    public String toString() {
        return "WebElement [id= " + id
                + " bfstree=" + bfstree + ",\n\t idsBfstree= " + idBfsTree
                + ", nodeTag=" + nodeTag + ", parentPath=" + parentPath
                + ", cssParentPath=" + parentCSSPath + ",\n\t position="
                + position + ", size=" + size + ", \n\t text=" + text + "]";
    }

    @JsonIgnore
    public Set<String> getUrls() {
        return getAnchors().keySet();
    }

    @JsonIgnore
    public WebPage getWebPage() {
        return webPage;
    }

    @JsonIgnore
    public void setWebPage(WebPage webPage) {
        this.webPage = webPage;

        for (WebElement we : children) {
            we.setWebPage(webPage);
        }
    }

    @Override
    public int hashCode() {
        return (id + webPage.getUrl()).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(WebElement.class)) {
            return id
                    .equals(((WebElement) obj).id);
        }

        return super.equals(obj);
    }

    @Override
    public int compareTo(WebElement o) {
        return id.compareTo(o.id);
    }

    @Deprecated
    @JsonIgnore
    public String getHtml() {
        String html = "";

        html += "<" + this.nodeTag;
        for (String attribute : this.attributes.keySet()) {
            html += " " + attribute + "=\"" + this.attributes.get(attribute)
                    + "\"";
        }
        html += ">";

        for (WebElement child : this.children) {
            if (!child.displayed)
                continue;

            html += child.getHtml();
        }

        html += this.text;
        html += "</" + this.nodeTag + ">";

        return html;
    }

    @JsonIgnore
    public Map<String, String> getAnchors() {
        if (bfstree == null) {
            getBFSTree();
        }

        return this.urls;
    }
}
