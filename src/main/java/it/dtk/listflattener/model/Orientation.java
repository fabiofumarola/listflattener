package it.dtk.listflattener.model;

public enum Orientation {
	Undefined, Vertical, Horizontal, Merged
}
