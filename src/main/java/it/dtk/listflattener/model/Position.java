package it.dtk.listflattener.model;

import it.dtk.listflattener.utils.Utils;

import com.infomatiq.jsi.Point;
import com.infomatiq.jsi.Rectangle;

import java.io.Serializable;

public class Position implements Serializable{

	private int bottom, left, right, top;
	
	public Position() {
	}

	public Position(int bottom, int left, int right, int top) {
		super();
		this.bottom = bottom;
		this.left = left;
		this.right = right;
		this.top = top;
	}

	public int getBottom() {
		return bottom;
	}

	public void setBottom(int bottom) {
		this.bottom = bottom;
	}

	public int getLeft() {
		return left;
	}

	public void setLeft(int left) {
		this.left = left;
	}

	public int getRight() {
		return right;
	}

	public void setRight(int right) {
		this.right = right;
	}

	public int getTop() {
		return top;
	}

	public void setTop(int top) {
		this.top = top;
	}

	@Override
	public String toString() {
		return "Position [bottom=" + bottom + ", left=" + left + ", right="
				+ right + ", top=" + top + "]";
	}

	public PositionType physicalPosition(Position b) {
		Rectangle aRectangle = Utils.getRectangle(this);
		Rectangle bRectangle = Utils.getRectangle(b);

		Point aCentre = aRectangle.centre();
		Point bCentre = bRectangle.centre();

		if (aCentre.y > bCentre.y) {
			if (aCentre.x > bCentre.x)
				return PositionType.below_right;
			else if (aCentre.x < bCentre.x)
				return PositionType.below_left;
			else
				return PositionType.below;
		} else if (aCentre.y < bCentre.y) {
			if (aCentre.x > bCentre.x)
				return PositionType.above_right;
			else if (aCentre.x < bCentre.x)
				return PositionType.above_left;
			else
				return PositionType.above;
		} else if (aCentre.y == bCentre.y) {
			if (aCentre.x < bCentre.x)
				return PositionType.left;
			else if (aCentre.x > bCentre.x)
				return PositionType.right;
			else
				return PositionType.overlap;
		}

		return PositionType.undefined;
	}
}
