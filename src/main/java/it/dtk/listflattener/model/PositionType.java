package it.dtk.listflattener.model;

public enum PositionType {
	above, above_right, right, below_right, below, below_left, left, above_left, overlap, undefined;
}
