package it.dtk.listflattener.model;

import java.io.Serializable;
import java.util.*;

public class WebPage implements Serializable{
	
	private String url;

    private String redirectUrl;
	
	private WebElement traversedHtml;
	
    private long timeStamp;
    
	private List<WebList> webLists;
	
    private Integer nextWebListId = 0;
    
    private String hash;
    
    private Set<WebElement> outLinks;
    
    private Set<WebElement> inLinks;

	public WebPage(String url, long timeStamp) {
		this.url = url;
        this.timeStamp = timeStamp;
        this.hash = EntityConst.LF_HASH.getHash(url);
        inLinks = new HashSet<>();
	}

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getUrl() {
		return this.url;
	}

    public String getHash() {
        return hash;
    }

    public String getKey() {
        return hash;
    }

    public int getNextWebListId() {
        return nextWebListId++;
    }

    public List<WebList> getExtensionLists() {
        List<WebList> extensionLists = new ArrayList<>();

        for (WebList wl : webLists) {
            if (wl.isExtendedWebList()) {
                extensionLists.add(wl);
            }
        }

        return extensionLists;
    }

	public WebElement getTraversedHtml() {
		return this.traversedHtml;
	}

    public long getTimeStamp() {
        return this.timeStamp;
    }

	public void setTraversedHtml(WebElement traversedHtml) {
		this.traversedHtml = traversedHtml;
		this.traversedHtml.setWebPage(this);
	}

	public List<WebList> getWebLists() {
		return this.webLists;
	}

	public void setWebLists(List<WebList> webLists) {
		this.webLists = webLists;
	}

	public void removeListWithoutUrls() {
		List<WebList> listWithUrl = new ArrayList<>();

		for (WebList webList : webLists) {

			if (!webList.getUrls().isEmpty())
				listWithUrl.add(webList);
		}

		webLists = listWithUrl;
	}

	public void sortWebList() {
		Collections.sort(webLists, new Comparator<WebList>() {

			@Override
			public int compare(WebList o1, WebList o2) {
				return o2.size() - o1.size();
			}
		});
	}

	@Override
	public int hashCode() {
		return url.hashCode();
	}

    public Map<String, String> getInLinkPairs() {
        Map<String, String> inLinkPairs = new HashMap<>();

        for (WebElement webElement : this.getInLinks()) {
            for (String text : webElement.getAnchors().values()) {
                inLinkPairs.put(webElement.getWebPage().getUrl(), text);
            }
        }

        return inLinkPairs;
    }

    public Map<String, String> getOutLinkPairs() {
        Map<String, String> outLinkPairs = new HashMap<>();

        for (WebElement webElement : this.getOutLinks()) {
            for (Map.Entry<String, String> entry : webElement.getAnchors().entrySet()) {
                outLinkPairs.put(entry.getKey(), entry.getValue());
            }
        }

        return outLinkPairs;
    }

    public Set<WebElement> getOutLinks() {
        if (this.outLinks == null) {
            this.outLinks = new HashSet<>();

            for (WebList webList : this.webLists) {
                for (WebElement webElement : webList.getWebElements()) {
                    this.outLinks.add(webElement);
                }
            }
        }

        return this.outLinks;
    }

    public Set<WebElement> getInLinks() {
        return this.inLinks;
    }
}
