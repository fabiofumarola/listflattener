package it.dtk.listflattener;

import it.dtk.listflattener.model.Response;
import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebList;
import it.dtk.listflattener.model.WebPage;
import it.dtk.listflattener.utils.Distance;
import it.dtk.utils.URLNormalizer;
import it.dtk.utils.UnixEpoch;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * @author batman this class allows the extraction of web list spanning multiple
 *         web pages. It can be considered as a web site flattener. Given an url
 *         a tagfactor value: 1. it extract the set S of the list in the web
 *         page 2. for each list x discovered in S for each list y in S - x for
 *         each record r in y get the url u in r extractlist from u if exists a
 *         list x_u which is spatially close to x and whose records look similar
 *         then sets that x via r link to x_u
 */
public class ListFlattener {

    private static long timeStamp = UnixEpoch.getTimeStamp();
    private static Map<String, WebPage> dbPages = new HashMap<>();

    private static String startUrl;

    /**
     * @param url
     * @param recordUrl
     * @param tagFactor
     * @param structSim
     * @return
     * @throws IOException
     */
    public static WebPage extract(String url, String recordUrl,
                                  double tagFactor, double structSim, boolean onlyListWithUrls)
            throws Exception {

        startUrl = url;

        WebPage page = ListExtractor.extract(url, tagFactor, timeStamp,
                onlyListWithUrls);
        dbPages.put(url, page);

        WebList toBeExtended = getListContaining(page.getWebLists(), recordUrl);
        Set<WebList> searchSpace = new HashSet<>(page.getWebLists());
        searchSpace.remove(toBeExtended);
        flatten(toBeExtended, searchSpace, tagFactor, structSim, onlyListWithUrls);

        findInLinks(page);

        return page;
    }

    public static WebPage extract(String url, double tagFactor, double structSim, boolean onlyListWithUrls)
            throws Exception {

        startUrl = url;
        WebPage page = ListExtractor.extract(url, tagFactor, timeStamp, onlyListWithUrls);
        page.sortWebList();

        dbPages.put(url, page);

        for (WebList webList : page.getWebLists()) {
            Set<WebList> searchSpace = new HashSet<>(page.getWebLists());
            searchSpace.remove(webList);
            flatten(webList, searchSpace, tagFactor, structSim, onlyListWithUrls);
        }

        findInLinks(page);

        return page;
    }

    /**
     * @param webList
     * @param searchSpace
     * @param tagFactor
     * @param structSim
     */
    private static void flatten(WebList webList, Set<WebList> searchSpace,
                                double tagFactor, double structSim, boolean onlyListWithUrls) throws Exception {

        Set<String> visitedUrls = new HashSet<>();

        System.out.println("Finding multipage list for: ");
        webList.print();

        // for each list y in S - x
        for (WebList candidate : searchSpace) {
            // for each record r in y
            for (WebElement record : candidate.getWebElements()) {
                for (String u : record.getUrls()) {
                    if (u.startsWith("javascript:") || u.endsWith("#")) {
                        continue;
                    }

                    String domain = URLNormalizer.getBaseUrl(startUrl);
                    String url = URLNormalizer.normalize(domain, u);

                    if (!domain.equalsIgnoreCase(URLNormalizer.getBaseUrl(url))) {
                        continue;
                    }

                    if (visitedUrls.contains(url)) {
                        // skip visiting this url
                        continue;
                    }

                    WebPage recordPage;

                    if (dbPages.containsKey(url)) {
                        recordPage = dbPages.get(url);
                    } else {
                        try {
                            recordPage = ListExtractor.extract(url, tagFactor,
                                    timeStamp, onlyListWithUrls);
                            dbPages.put(url, recordPage);
                            recordPage.sortWebList();
                        } catch (Exception e) {
                            System.err.println(e.getMessage());
                            System.err.println("\tURL was: " + url);

                            // go to the next url
                            continue;
                        }
                    }

                    visitedUrls.add(url);

                    for (WebList recordPageWL : recordPage.getWebLists()) {
                        if (areMultiPageLists(webList, recordPageWL, tagFactor,
                                structSim)) {
                            System.out
                                    .println("the Web List is extended using the record containing the url: "
                                            + url);
                            webList.getExtensionLists().put(record,
                                    recordPageWL);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param webList
     * @param candidate
     * @param tagFactor
     * @param structSim
     * @return they should have the elements structurally similar they should be
     * close as distance
     */
    private static boolean areMultiPageLists(WebList webList,
                                             WebList candidate, double tagFactor, double structSim) {

        double structSimilarity = webList.partialStructuralDistance(candidate);
        boolean typeOrientation = webList.getOrientation() == candidate
                .getOrientation();
        double tagPathDistance = Distance.tagPathDistance(webList, candidate);

        // double eucledianDistance = Distance.eucledianDistance(webList,
        // candidate);
        // int candidateUrlDepth = Utils.countOccurence(candidateUrl, "/") - 2;
        // int urlDepth = Utils.countOccurence(startUrl, "/") - 2;
        // boolean sameDepth = candidateUrlDepth == urlDepth;

        return typeOrientation && (structSimilarity < structSim)
                && (tagPathDistance < tagFactor);
    }

    /**
     * @param webLists
     * @param recordUrl
     * @return
     */
    private static WebList getListContaining(List<WebList> webLists,
                                             String recordUrl) {

        WebList toBeExtended = null;
        int listSize = 0;

        // find the candidate List
        for (WebList wl : webLists) {
            Set<String> setUrls = URLNormalizer.normalize(
                    URLNormalizer.getBaseUrl(recordUrl), wl.getUrls());

            if (setUrls.contains(recordUrl)) {
                if (listSize < wl.getWebElements().size()) {
                    toBeExtended = wl;
                    listSize = wl.getWebElements().size();
                }
            }
        }

        if (toBeExtended == null) {
            throw new RuntimeException(
                    "L'url del record non e` presente all'interno di questa pagina.");
        }

        return toBeExtended;
    }

    private static void findInLinks(WebPage webPage) {
        for (WebList webList : webPage.getExtensionLists()) {
            for (WebElement webElement : webList.getWebElements()) {
                if (webElement.getUrls()
                        .contains(webList.getWebPage().getUrl())) {
                    webPage.getInLinks().add(webElement);
                }
            }
        }
    }


    public static void close() {
        Response.traverser.close();
    }
}
