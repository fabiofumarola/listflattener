package it.dtk.listflattener;

import it.dtk.listflattener.filter.HorizontalListFilter;
import it.dtk.listflattener.filter.ListFilter;
import it.dtk.listflattener.filter.StructuralListFilter;
import it.dtk.listflattener.filter.VerticalListFilter;
import it.dtk.listflattener.model.WebElement;
import it.dtk.listflattener.model.WebList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author
 * 
 */
class ListsFinder {

	private List<WebList> aligned;
	private List<WebElement> notAligned;

	/**
	 * @param webElement
	 * @param tagFactor
	 *            Given a web element discover the vertical and horizontal lists
	 */
	public ListsFinder(WebElement webElement, double tagFactor) {

		ListFilter vFilter = new VerticalListFilter(webElement);
		vFilter.filter();
		ListFilter hFilter = new HorizontalListFilter(webElement);
		hFilter.filter();

		Map<Integer, WebElement> setNotAligned = new HashMap<>();
		setNotAligned.putAll(vFilter.getNotAligned());
		setNotAligned.putAll(hFilter.getNotAligned());

		StructuralListFilter structuralFilter = new StructuralListFilter(
				webElement);

		for (WebList l : vFilter.getAligned()) {
			structuralFilter.filter(l, tagFactor);
		}

		for (WebList l : hFilter.getAligned()) {
			structuralFilter.filter(l, tagFactor);
		}

		aligned = structuralFilter.getAligned();

		setNotAligned.putAll(structuralFilter.getNotAligned());

		notAligned = new ArrayList<>(setNotAligned.values());
	}

	public List<WebList> getAligned() {
		return aligned;
	}

	public List<WebElement> getNotAligned() {
		return notAligned;
	}

}
