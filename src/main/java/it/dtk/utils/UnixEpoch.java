package it.dtk.utils;

/*
 * Author: Michele Damiano Torelli
 * Project: ListFlattener
 * Date: 17/07/13
 * Time: 10:52
 */
public class UnixEpoch {
    public static long getTimeStamp() {
        return (System.currentTimeMillis() / 1000L);
    }
}
