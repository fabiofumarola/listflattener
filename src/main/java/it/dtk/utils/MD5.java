package it.dtk.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Author: Michele Damiano Torelli
 * Project: ListFlattener
 * Date: 15/07/13
 * Time: 10:15
 */
public class MD5 implements LFHash {

    @Override
    public byte[] digest(String string) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");

            md.reset();
            md.update(string.getBytes());

            return md.digest();
        } catch (NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
        }

        return null;
    }

    @Override
    public String toString(byte[] digest) {
        BigInteger bigInt = new BigInteger(1, digest);
        String hashText = bigInt.toString(16);

        // Now we need to zero pad it if you actually want the full 32 chars.
        while(hashText.length() < 32){
            hashText = "0" + hashText;
        }

        return hashText;
    }

    public static MD5 getInstance() {
        if (instance == null) {
            instance = new MD5();
        }

        return instance;
    }

    public String getHash(String string) {
        if (instance == null)
             instance = MD5.getInstance();

        byte[] digest = instance.digest(string);
        return instance.toString(digest);
    }

    private static MD5 instance;
}
