package it.dtk.utils;

/**
 * Author: Michele Damiano Torelli
 * Project: ListFlattener
 * Date: 15/07/13
 * Time: 10:06
 */
public interface LFHash {

	public byte[] digest(String string);

    public String toString(byte[] digest);

    public String getHash(String string);

}
