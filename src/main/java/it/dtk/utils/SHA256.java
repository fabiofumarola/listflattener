package it.dtk.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Author: Michele Damiano Torelli
 * Project: ListFlattener
 * Date: 17/07/13
 * Time: 10:11
 */
public class SHA256 implements LFHash {

    @Override
    public byte[] digest(String string) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");

            md.reset();
            md.update(string.getBytes());

            return md.digest();
        } catch (NoSuchAlgorithmException nsae) {
            nsae.printStackTrace();
        }

        return null;
    }

    @Override
    public String toString(byte[] digest) {
        BigInteger bigInt = new BigInteger(1, digest);
        String hashText = bigInt.toString(16);

        // Now we need to zero pad it if you actually want the full 64 chars.
        while(hashText.length() < 64){
            hashText = "0" + hashText;
        }

        return hashText;
    }

    public static SHA256 getInstance() {
        if (instance == null) {
            instance = new SHA256();
        }

        return instance;
    }

    public String getHash(String string) {
        if (instance == null)
            instance = SHA256.getInstance();

        byte[] digest = instance.digest(string);
        return instance.toString(digest);
    }

    private static SHA256 instance;
}
