package it.dtk.utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Author: Michele Damiano Torelli
 * Project: ListFlattener
 * Date: 25/07/13
 * Time: 09:41
 */
public class URLNormalizer {
    public static String getBaseUrl(String url) {
        String lowerCaseUrl = url.toLowerCase();

        if (!lowerCaseUrl.startsWith("http") && !lowerCaseUrl.startsWith("https")) {
            return lowerCaseUrl.split("/")[0];
        } else {
            return lowerCaseUrl.split("/")[2];
        }
    }

    public static String normalize(String url) {
        String output = url.toLowerCase();

        if (!output.endsWith("/"))
            output += "/";

        if (!output.startsWith("http") && !output.startsWith("https"))
            output = "http://" + output;

        return output;
    }

    public static String normalize(String baseUrl, String url) {
        String lowerCaseUrl = url.toLowerCase();

        if (isAbsolute(lowerCaseUrl) && !lowerCaseUrl.startsWith("http") && !lowerCaseUrl.startsWith("https")) {
            return normalize(getBaseUrl(baseUrl)) + lowerCaseUrl.substring(1);
        } else if (isRelative(lowerCaseUrl)) {
            return normalize(baseUrl) + lowerCaseUrl;
        } else {
            return lowerCaseUrl;
        }
    }

    public static Set<String> normalize(String baseUrl, Set<String> urls) {
        Set<String> result = new HashSet<>();

        for (String url : urls) {
            result.add(normalize(baseUrl, url));
        }

        return result;
    }

    public static boolean isAbsolute(String url) {
        if (url.startsWith("/") || url.startsWith("//"))
            return true;

        String lowerCaseUrl = url.toLowerCase();

        return lowerCaseUrl.startsWith("http")
                || lowerCaseUrl.startsWith("https");
    }

    public static boolean isNormalized(String url) {
        String lowerCaseUrl = url.toLowerCase();

        return lowerCaseUrl.endsWith("/")
                && (lowerCaseUrl.startsWith("http") || lowerCaseUrl.startsWith("https"));
    }

    public static boolean isRelative(String url) {
        String lowerCaseUrl = url.toLowerCase();

        return !lowerCaseUrl.startsWith("/")
                && !lowerCaseUrl.startsWith("//")
                && !lowerCaseUrl.startsWith("http")
                && !lowerCaseUrl.startsWith("https");
    }
}
