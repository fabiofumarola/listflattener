package it.dtk.utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*   Copyright 2004 The Apache Software Foundation
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *  limitations under the License.
 */

public class Levenshtein {
	// ****************************
	// Get minimum of three values
	// ****************************

	private static int minimum(int a, int b, int c) {
		int mi = a;
		if (b < mi)
			mi = b;
		if (c < mi)
			mi = c;
		return mi;
	}

	// *****************************
	// Compute Levenshtein similiarity
	// *****************************
	public static double similiarity(String s, String t) {
		double distance = distance(s, t);

		return 1 - distance / Math.max(s.length(), t.length());
	}

	// *****************************
	// Compute Levenstein the Normalized distance
	// *****************************
	public static double normalizedDistance(String s, String t) {
		double distance = distance(s, t);
		return distance / ((s.length() + t.length()) / 2);
	}

	// *****************************
	// Compute Levenstein distance
	// *****************************
	public static int distance(String s, String t) {
		int d[][]; // matrix
		int n; // length of s
		int m; // length of t
		int i; // iterates through s
		int j; // iterates through t
		char s_i; // ith character of s
		char t_j; // jth character of t
		int cost; // cost

		// Step 1
		n = s.length();
		m = t.length();
		if (n == 0)
			return m;
		if (m == 0)
			return n;
		d = new int[n + 1][m + 1];

		// Step 2
		for (i = 0; i <= n; i++)
			d[i][0] = i;
		for (j = 0; j <= m; j++)
			d[0][j] = j;

		// Step 3
		for (i = 1; i <= n; i++) {
			s_i = s.charAt(i - 1);

			// Step 4
			for (j = 1; j <= m; j++) {
				t_j = t.charAt(j - 1);

				// Step 5
				if (s_i == t_j)
					cost = 0;
				else
					cost = 1;

				// Step 6
				d[i][j] = minimum(d[i - 1][j] + 1, d[i][j - 1] + 1,
						d[i - 1][j - 1] + cost);
			}
		}

		// Step 7
		return d[n][m];
	}

	/**
	 * @author Barber/Wenninger
	 */
	public static double weightedWordDistance(String s1, String s2, int s1Max,
			int s2Max) {
		String[] s1Split, s2Split, longArr, shortArr;
		s1Split = s1.split(" ");
		s2Split = s2.split(" ");

		double maxResult = 0.0;
		double result = 0.0;
		int longMax, shortMax;
		if (s1Split.length > s2Split.length) {
			longArr = s1Split;
			longMax = s1Max;
			shortArr = s2Split;
			shortMax = s2Max;
		} else {
			longArr = s2Split;
			longMax = s2Max;
			shortArr = s1Split;
			shortMax = s1Max;
		}
		// let's make this O(m + n) instead of O(mn)
		HashMap<String, Integer> longMap = new HashMap<>(), shortMap = new HashMap<>();
		for (String s : longArr) {
			int i = 0;
			if (longMap.containsKey(s))
				i = longMap.get(s);

			i++;
			longMap.put(s, i);

		}
		for (String s : shortArr) {
			int i = 0;
			if (shortMap.containsKey(s))
				i = shortMap.get(s);

			i++;
			shortMap.put(s, i);
		}
		// get max score
		for (String s : longMap.keySet()) {
			int num = longMap.get(s);
			maxResult += scoreWord(s, longMax, num);

		}
		for (String s : shortMap.keySet()) {
			int num = shortMap.get(s);
			maxResult += scoreWord(s, shortMax, num);
		}
		// remove matches
		for (String s : longArr) {
			if (shortMap.containsKey(s)) { // remove from both
				int shortNum, longNum;
				shortNum = shortMap.get(s);
				longNum = longMap.get(s);
				shortNum--;
				longNum--;
				if (shortNum == 0)
					shortMap.remove(s);
				else
					shortMap.put(s, shortNum);
				if (longNum == 0)
					longMap.remove(s);
				else
					longMap.put(s, longNum);
			}

		}

		// count what's left in the longMap and in the shortArr
		for (String s : longMap.keySet()) {
			int num = longMap.get(s);
			result += scoreWord(s, longMax, num);

		}
		for (String s : shortMap.keySet()) {
			int num = shortMap.get(s);
			result += scoreWord(s, shortMax, num);
		}
		return result / maxResult;

	}

	public static double scoreWord(String word, int maxWeight, int num) {
		String pattern = "(\\d+)";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(word);
		int score;
		if (!m.find()) {
			System.err.println("Tag with no score");
			return -1;
		} else {
			score = Integer.parseInt(word.substring(m.start(), m.end()));
			score++;
		}
		return ((double) (score)) / maxWeight * num;
	}

}