package it.dtk.utils;

/**
 * Author: Michele Damiano Torelli
 * Project: ListFlattener
 * Date: 15/07/13
 * Time: 15:15
 */
public class StringNormalizer {
    public static String normalize(String text, int length) {
        return normalize(text, length, DEFAULT_PADDING_CHAR);
    }

    public static String normalize(String text, int length, char paddingChar) {
        if (text.length() > length) {
            throw new IllegalArgumentException("String length longer than normalized length");
        } else if (text.length() == length) {
            return text;
        } else {
            int difference = length - text.length();
            String normalizedString = "";

            do {
                normalizedString += String.valueOf(paddingChar);
                difference--;
            } while(difference != 0);

            return (normalizedString += text);
        }
    }

    private static final char DEFAULT_PADDING_CHAR = '0';
}
