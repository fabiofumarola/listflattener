package it.dtk.listflattener;

import it.dtk.listflattener.model.WebList;
import it.dtk.listflattener.model.WebPage;
import it.dtk.traverser.TraverserFactory;
import it.dtk.utils.UnixEpoch;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class ListExtractorTest {

	public static void main(String[] args) {
		String webSite = "http://ec.europa.eu/research/participants/portal/desktop/en/opportunities/h2020/#c,calls=level3/t/EU.1./0/1/1/default-group&level4/t/EU.1.1./0/1/1/default-group&level4/t/EU.1.2./0/1/1/default-group&level4/t/EU.1.3./0/1/1/default-group&level4/t/EU.1.4./0/1/1/default-group&level3/t/EU.2./0/1/1/default-group&level4/t/EU.2.1./0/1/1/default-group&level5/t/EU.2.1.1./0/1/1/default-group&level5/t/EU.2.1.2./0/1/1/default-group&level5/t/EU.2.1.3./0/1/1/default-group&level5/t/EU.2.1.4./0/1/1/default-group&level5/t/EU.2.1.5./0/1/1/default-group&level5/t/EU.2.1.6./0/1/1/default-group&level4/t/EU.2.2./0/1/1/default-group&level4/t/EU.2.3./0/1/1/default-group&level3/t/EU.3./0/1/1/default-group&level4/t/EU.3.1./0/1/1/default-group&level4/t/EU.3.2./0/1/1/default-group&level4/t/EU.3.3./0/1/1/default-group&level4/t/EU.3.4./0/1/1/default-group&level4/t/EU.3.5./0/1/1/default-group&level4/t/EU.3.6./0/1/1/default-group&level4/t/EU.3.7./0/1/1/default-group&level3/t/EU.4./0/1/1/default-group&level3/t/EU.5./0/1/1/default-group&level3/t/EU.7./0/1/1/default-group&level2/t/Euratom/0/1/1/default-group&hasForthcomingTopics/t/true/1/1/0/default-group&hasOpenTopics/t/true/1/1/0/default-group&allClosedTopics/t/true/0/1/0/default-group&+PublicationDateLong/asc";
		double tagFactor = 0.4d;

		if (args.length != 1) {
			System.out.println("Usage: ListExtractorTest <website_url> ");

			//System.exit(1);
		} else {
			webSite = args[0];
		}

		try {
            //String ip = "65.52.156.108:15000";
            String ip = "193.204.187.132:15000";
            TraverserFactory.vpnUrl = "http://" + ip + "/traverseAsync?url=";

			double start = System.currentTimeMillis();
			WebPage webPage = ListExtractor.extract(webSite, tagFactor, UnixEpoch.getTimeStamp(), true);
           // ListExtractor.close();
            //webPage = ListExtractor.extract(webSite, tagFactor, UnixEpoch.getTimeStamp(), true);
            ListExtractor.close();
			double time = (System.currentTimeMillis() - start) / 1000;

			System.out.println("Total time = " + time + " s");

			System.out.println("Lists found: " + webPage.getWebLists().size());

			for (WebList l : webPage.getWebLists()) {
				l.print();
				System.out.println("#######################################\n");
			}

            //ListExtractor.close();
		} catch (Exception e ) {
			e.printStackTrace();
		}
	}

}
