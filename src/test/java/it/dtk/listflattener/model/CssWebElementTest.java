package it.dtk.listflattener.model;

import it.dtk.listflattener.ListExtractor;
import it.dtk.utils.UnixEpoch;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class CssWebElementTest {

	
	public void test(){
		String webSite="http://www.uniba.it/ricerca/dipartimenti/informatica/dipartimento/personale/docenti";
		double tagFactor = 0.4d;
		try {
			double start = System.currentTimeMillis();
			WebPage webPage = ListExtractor.extract(webSite, tagFactor, UnixEpoch.getTimeStamp(), true);
			double time = (System.currentTimeMillis() - start) / 1000;

			System.out.println("Total time = " + time + " s");

			System.out.println("Lists found: " + webPage.getWebLists().size());

			for (WebList l : webPage.getWebLists()) {
				for(WebElement e: l.getWebElements()){
					System.out.println(e.getAttributes());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		CssWebElementTest css = new CssWebElementTest();
		css.test();
	}
}
