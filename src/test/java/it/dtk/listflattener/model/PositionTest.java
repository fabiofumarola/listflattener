package it.dtk.listflattener.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PositionTest {

	@Test
	public void testOverlap() {
		Position a = new Position(400, 100, 300, 100);
		Position b = new Position(300, 200, 200, 200);

		System.out.println(a.physicalPosition(b));
		assertEquals(PositionType.overlap, a.physicalPosition(b));
	}

	@Test
	public void testBelow() {
		Position a = new Position(700, 100, 300, 500);
		Position b = new Position(300, 100, 300, 100);

		System.out.println(a.physicalPosition(b));
		assertEquals(PositionType.below, a.physicalPosition(b));
	}

	@Test
	public void testBelowLeft() {
		Position a = new Position(300, 100, 300, 200);
		Position b = new Position(199, 320, 350, 100);

		System.out.println(a.physicalPosition(b));
		assertEquals(PositionType.below_left, a.physicalPosition(b));
	}

	@Test
	public void testBelowRight() {
		Position a = new Position(300, 100, 300, 200);
		Position b = new Position(199, 10, 80, 100);

		System.out.println(a.physicalPosition(b));
		assertEquals(PositionType.below_right, a.physicalPosition(b));
	}

	@Test
	public void testAbove() {
		Position a = new Position(300, 100, 300, 100);
		Position b = new Position(700, 100, 300, 500);

		System.out.println(a.physicalPosition(b));
		assertEquals(PositionType.above, a.physicalPosition(b));
	}

	@Test
	public void testAboveRight() {
		Position a = new Position(199, 320, 350, 100);
		Position b = new Position(300, 100, 300, 200);

		System.out.println(a.physicalPosition(b));
		assertEquals(PositionType.above_right, a.physicalPosition(b));
	}

	@Test
	public void testAboveLeft() {
		Position a = new Position(199, 10, 80, 100);
		Position b = new Position(300, 100, 300, 200);

		System.out.println(a.physicalPosition(b));
		assertEquals(PositionType.above_left, a.physicalPosition(b));
	}

	@Test
	public void testLeft() {
		Position a = new Position(400, 100, 300, 200);
		Position b = new Position(400, 400, 600, 200);

		System.out.println(a.physicalPosition(b));
		assertEquals(PositionType.left, a.physicalPosition(b));
	}

	@Test
	public void testRight() {
		Position a = new Position(400, 400, 600, 200);
		Position b = new Position(400, 100, 300, 200);

		System.out.println(a.physicalPosition(b));
		assertEquals(PositionType.right, a.physicalPosition(b));
	}

}
