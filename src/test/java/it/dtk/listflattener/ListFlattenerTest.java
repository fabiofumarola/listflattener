package it.dtk.listflattener;

import it.dtk.listflattener.model.WebList;
import it.dtk.listflattener.model.WebPage;
import it.dtk.traverser.TraverserFactory;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class ListFlattenerTest {

    public static void main(String[] args) {
        String webSite = null;
        String recordUrl = null;
        double tagFactor = 0.4d;
        double structSim = 0.6d;

        if (args.length != 2) {
            System.out.println("Usage: ListFlattenerTest <website_url> <record_url>");
            System.exit(1);
        } else {
            webSite = args[0];
            recordUrl = args[1].toLowerCase();
        }

        try {

            String urlService = "http://193.204.187.132:15000/traverseAsync?url=";
            TraverserFactory.vpnUrl = urlService;

            WebPage page = ListFlattener.extract(webSite, recordUrl, tagFactor, structSim, true);

            for (WebList extensionList : page.getExtensionLists()) {
                extensionList.print();
            }
            ListExtractor.close();

        } catch (Exception e) {
            e.printStackTrace();

        }
    }

}
