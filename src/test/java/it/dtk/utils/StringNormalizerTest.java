package it.dtk.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Author: Michele Damiano Torelli
 * Project: ListFlattener
 * Date: 15/07/13
 * Time: 15:39
 */
public class StringNormalizerTest {

    @Test
    public void testDefaultNormalize() throws Exception {
        String expectedString = "00005";
        String normalizedString = StringNormalizer.normalize("5", 5);

        System.out.println();
        System.out.println("*** Default Normalize Test (zeroes padding) ***");
        System.out.println("Expected normalized string: " + expectedString);
        System.out.println("Normalized string: " + normalizedString);

        assertEquals(expectedString, normalizedString);
    }

    @Test
    public void testNormalize() throws Exception {
        String expectedString = "aaaaab";
        String normalizedString = StringNormalizer.normalize("b", 6, 'a');

        System.out.println();
        System.out.println("*** Normalize Test ***");
        System.out.println("Expected normalized string: " + expectedString);
        System.out.println("Normalized string: " + normalizedString);

        assertEquals(expectedString, normalizedString);
    }
}
