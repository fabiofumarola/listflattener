package it.dtk.utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * User: Michele Damiano Torelli Date: 17/07/13 Time: 10:22
 */
public class SHA256Test {
	private static final int ELEMENTS = 3000;

//	@Test
//	public void testGetHash() throws Exception {
//		// "SHA-256 Test" ->
//		// "c21ff307399949a7d12657e0915b524776788eeb507a6bebc00fab8b0641f7d7"
//		String text = "SHA-256 Test";
//		String expectedSha256Hash = "c21ff307399949a7d12657e0915b524776788eeb507a6bebc00fab8b0641f7d7";
//
//		String sha256Hash = SHA256.getInstance().getHash(text);
//
//		System.out.println();
//		System.out.println("*** SHA-256 Test ***");
//		System.out.println("Text: " + text);
//		System.out.println("Expected SHA-256 hash: " + expectedSha256Hash);
//		System.out.println("Obtained SHA-256 hash: " + sha256Hash);
//
//		assertEquals(expectedSha256Hash, sha256Hash);
//	}
//
//	@Test
//	public void testPerformance() throws Exception {
//		final long startTime = UnixEpoch.getTimeStamp();
//
//		for (int i = 0; i < ELEMENTS; i++) {
//			SHA256.getInstance().getHash(String.valueOf(i));
//		}
//
//		final long stopTime = UnixEpoch.getTimeStamp();
//
//		System.out.println();
//		System.out.println("*** SHA-256 Performance Test ***");
//		System.out.println("String array size: " + ELEMENTS);
//		System.out.println("Unix epoch start: " + startTime);
//		System.out.println("Unix epoch stop: " + stopTime);
//		System.out.println("Difference (secs): " + (stopTime - startTime));
//	}
//
//	@Test
//	public void testCollisions() throws Exception {
//		HashMap<String, Boolean> hashes = new HashMap<>(ELEMENTS);
//		int collisions = 0;
//
//		for (int i = 0; i < ELEMENTS; i++) {
//			String hash = SHA256.getInstance().getHash(String.valueOf(i));
//
//			if (hashes.get(hash) != null)
//				collisions++;
//			else
//				hashes.put(hash, true);
//		}
//
//		assertEquals(0, collisions);
//
//		System.out.println();
//		System.out.println("*** SHA-256 Collisions Test ***");
//		System.out.println("String array size: " + ELEMENTS);
//		System.out.println("Collisions: " + collisions);
//	}
//
//	@Test
//	public void testDigestUtils() {
//		String text = "SHA-256 Test";
//		String expectedSha256Hash = "c21ff307399949a7d12657e0915b524776788eeb507a6bebc00fab8b0641f7d7";
//
//		String sha256Hex = DigestUtils.sha256Hex(text);
//		System.out.println(sha256Hex);
//
//		assertEquals(expectedSha256Hash, sha256Hex);
//
//	}
//
//	@Test
//	public void testBase64() {
//		String url = "http://commons.apache.org/proper/commons-codec/apidocs/index.html?org/apache/commons/codec/binary/Base64.html";
//		// Get bytes from string
//		byte[] byteArray = Base64.encodeBase64(url.getBytes());
//
//		// Print the encoded byte array
//		//System.out.println(Arrays.toString(byteArray));
//
//		// Print the encoded string
//		String encodedString = new String(byteArray);
//
////		System.out.println(string + " = " + encodedString);
//
//		//decode the encoded string
//		byte[] decoded = Base64.decodeBase64(encodedString.getBytes());
//
//		System.out.println(url + " = " + new String(decoded));
//
//		String actual = new String(decoded);
//
//		assertEquals(url, actual);
//
//	}

}
