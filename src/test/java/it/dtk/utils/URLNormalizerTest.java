package it.dtk.utils;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Author: Michele Damiano Torelli
 * Project: ListFlattener
 * Date: 25/07/13
 * Time: 09:43
 */
public class URLNormalizerTest {
    @Test
    public void testGetBaseUrl() throws Exception {
        String url1 = "http://www.baritoday.it/cronaca/strage-via-niccolo.html";
        String url2 = "http://Www.BariToday.it/croNaca/strage-via-niccolo.html";
        String url3 = "http://baritoday.it/cronaca/strage-via-niccolo.html";
        String url4 = "www.baritoday.it/cronaca/strage-via-niccolo.html";
        String url5 = "baritoday.it/cronaca/strage-via-niccolo.html";
        String domainName1 = "www.baritoday.it";
        String domainName2 = "baritoday.it";

        assertEquals(domainName1, URLNormalizer.getBaseUrl(url1));
        assertEquals(domainName1, URLNormalizer.getBaseUrl(url2));
        assertEquals(domainName2, URLNormalizer.getBaseUrl(url3));
        assertEquals(domainName1, URLNormalizer.getBaseUrl(url4));
        assertEquals(domainName2, URLNormalizer.getBaseUrl(url5));
    }

    @Test
    public void testNormalize1() throws Exception {
        String url1 = "http://www.baritoday.it";
        String url2 = "https://www.baritoday.it";
        String url3 = "hTtp://Www.BARITodaY.iT";
        String url4 = "www.baritoday.it";
        String url5 = "www.baritoday.it/";
        String normalizedUrl1 = "http://www.baritoday.it/";
        String normalizedUrl2 = "https://www.baritoday.it/";

        assertEquals(normalizedUrl1, URLNormalizer.normalize(url1));
        assertEquals(normalizedUrl2, URLNormalizer.normalize(url2));
        assertEquals(normalizedUrl1, URLNormalizer.normalize(url3));
        assertEquals(normalizedUrl1, URLNormalizer.normalize(url4));
        assertEquals(normalizedUrl1, URLNormalizer.normalize(url5));
    }

    @Test
    public void testNormalize2() throws Exception {
        String baseUrl1 = "http://www.baritoday.it/pages/4/";
        String baseUrl2 = "https://www.baritoday.it/pages/4";
        String baseUrl3 = "hTtp://Www.BARITodaY.iT/paGes/4";
        String baseUrl4 = "www.baritoday.it/pages/4";
        String absolutePath = "/strage-via-niccolo.html";
        String relativePath = "stragi/strage-via-niccolo.html";
        String normalizedUrl1 = "http://www.baritoday.it/strage-via-niccolo.html";
        String normalizedUrl2 = "http://www.baritoday.it/pages/4/stragi/strage-via-niccolo.html";
        String normalizedUrl3 = "https://www.baritoday.it/pages/4/stragi/strage-via-niccolo.html";

        assertEquals(normalizedUrl1, URLNormalizer.normalize(baseUrl1, absolutePath));
        assertEquals(normalizedUrl1, URLNormalizer.normalize(baseUrl2, absolutePath));
        assertEquals(normalizedUrl1, URLNormalizer.normalize(baseUrl3, absolutePath));
        assertEquals(normalizedUrl1, URLNormalizer.normalize(baseUrl4, absolutePath));

        assertEquals(normalizedUrl2, URLNormalizer.normalize(baseUrl1, relativePath));
        assertEquals(normalizedUrl3, URLNormalizer.normalize(baseUrl2, relativePath));
        assertEquals(normalizedUrl2, URLNormalizer.normalize(baseUrl3, relativePath));
        assertEquals(normalizedUrl2, URLNormalizer.normalize(baseUrl4, relativePath));
    }

    @Test
    public void testIsAbsolute() throws Exception {
        String url1 = "http://www.baritoday.it";
        String url2 = "https://www.baritoday.it";
        String url3 = "http://www.baritoday.it/";
        String url4 = "https://www.baritoday.it/";
        String url5 = "http://www.baritoday.it/pages/4/";
        String url6 = "https://www.baritoday.it/pages/4/";
        String url7 = "www.baritoday.it/pages/4";
        String url8 = "/strage-via-niccolo.html";
        String url9 = "//strage-via-niccolo.html";
        String url10 = "stragi/strage-via-niccolo.html";

        assertTrue(URLNormalizer.isAbsolute(url1));
        assertTrue(URLNormalizer.isAbsolute(url2));
        assertTrue(URLNormalizer.isAbsolute(url3));
        assertTrue(URLNormalizer.isAbsolute(url4));
        assertTrue(URLNormalizer.isAbsolute(url5));
        assertTrue(URLNormalizer.isAbsolute(url6));
        assertFalse(URLNormalizer.isAbsolute(url7));
        assertTrue(URLNormalizer.isAbsolute(url8));
        assertTrue(URLNormalizer.isAbsolute(url9));
        assertFalse(URLNormalizer.isAbsolute(url10));

    }

    @Test
    public void testIsRelative() throws Exception {
        String url1 = "http://www.baritoday.it";
        String url2 = "https://www.baritoday.it";
        String url3 = "http://www.baritoday.it/";
        String url4 = "https://www.baritoday.it/";
        String url5 = "http://www.baritoday.it/pages/4/";
        String url6 = "https://www.baritoday.it/pages/4/";
        String url7 = "www.baritoday.it/pages/4";
        String url8 = "/strage-via-niccolo.html";
        String url9 = "//strage-via-niccolo.html";
        String url10 = "stragi/strage-via-niccolo.html";

        assertFalse(URLNormalizer.isRelative(url1));
        assertFalse(URLNormalizer.isRelative(url2));
        assertFalse(URLNormalizer.isRelative(url3));
        assertFalse(URLNormalizer.isRelative(url4));
        assertFalse(URLNormalizer.isRelative(url5));
        assertFalse(URLNormalizer.isRelative(url6));
        assertTrue(URLNormalizer.isRelative(url7));
        assertFalse(URLNormalizer.isRelative(url8));
        assertFalse(URLNormalizer.isRelative(url9));
        assertTrue(URLNormalizer.isRelative(url10));
    }

    @Test
    public void testIsNormalized() throws Exception {
        String url1 = "http://www.baritoday.it";
        String url2 = "http://www.baritoday.it/";
        String url3 = "http://www.baritoday.it/pages/4/";
        String url4 = "http://www.baritoday.it/pages/4";
        String url5 = "www.baritoday.it/pages/4";
        String url6 = "www.baritoday.it/pages/4/";

        assertFalse(URLNormalizer.isNormalized(url1));
        assertTrue(URLNormalizer.isNormalized(url2));
        assertTrue(URLNormalizer.isNormalized(url3));
        assertFalse(URLNormalizer.isNormalized(url4));
        assertFalse(URLNormalizer.isNormalized(url5));
        assertFalse(URLNormalizer.isNormalized(url6));
    }
}
