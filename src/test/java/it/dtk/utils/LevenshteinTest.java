package it.dtk.utils;

import it.dtk.listflattener.utils.TokenBasedDistance;

import org.junit.Test;

public class LevenshteinTest {

	@Test
	public void normalizedDistance() {
		String a = "tr/td/td/a";
		String b = "tr";

		System.out.println(TokenBasedDistance.normalizedDistance(a, b, "/"));
	}

	@Test
	public void similiarity() {
		String a = "div/div/div/ul/ul/ul/li/li/li/li/li/li/li/li/a/a/ul/a/a/a/div/a/a/img/span/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/div/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/span/span/span/iframe";
		String b = "div/div/div/div/div/div/div/div/div/div/div/div/ul/div/div/div/div/div/div/div/div/br/div/br/div/div/div/p/iframe/iframe/h1/span/span/span/li/li/li/li/li/li/li/li/form/div/h1/p/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/ul/div/div/div/div/div/h4/ul/h4/div/div/div/div/div/h4/ul/div/div/div/div/div/div/a/a/img/a/a/a/a/a/a/div/a/a/input/input/p/div/ul/a/a/h1/p/ul/a/h1/p/ul/div/a/h1/p/ul/a/h1/p/ul/a/h1/p/ul/a/h1/p/div/ul/a/h1/p/ul/a/h1/p/ul/a/h1/p/ul/a/h1/p/ul/a/h1/p/ul/a/h1/p/div/ul/a/h1/p/ul/a/h1/p/ul/a/h1/p/ul/li/li/li/li/li/li/li/h3/ul/h3/div/div/h4/ul/a/h4/ul/div/div/div/div/div/div/div/ins/div/div/div/div/li/li/li/li/li/li/li/li/li/li/li/li/li/li/div/div/br/h4/ul/h4/ul/h4/ul/h4/ul/li/li/li/li/li/li/h4/ul/h4/ul/h5/ul/h5/ul/h5/ul/p/a/a/ul/span/div/h4/ul/ul/h4/ul/ul/li/li/li/li/li/li/li/li/li/img/a/a/i/li/img/a/a/i/li/li/img/a/a/i/li/img/a/a/i/li/li/img/a/a/i/li/img/a/a/i/a/li/img/a/a/i/li/img/a/a/i/li/li/img/a/a/i/li/img/a/a/i/li/img/a/a/i/li/li/img/a/a/i/a/img/a/a/i/li/img/a/a/i/li/img/a/a/i/li/li/a/a/a/a/a/a/li/li/li/li/li/h4/ul/h4/ul/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/div/div/div/div/div/div/div/ins/img/iframe/img/b/a/b/a/b/a/b/a/b/a/b/a/b/a/b/a/b/a/b/a/b/a/b/a/b/a/b/a/a/a/div/div/li/li/li/li/li/li/li/li/li/li/li/li/a/a/a/a/a/a/a/a/a/a/a/a/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/a/div/p/span/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/li/a/a/a/a/a/a/a/a/a/b/a/b/a/b/a/b/a/b/a/b/a/b/a/a/span/b/a/a/b/a/a/b/a/b/a/b/a/a/b/a/a/b/a/a/b/a/span/b/a/b/a/b/a/b/a/b/b/b/a/b/a/b/a/b/a/b/a/li/li/li/li/li/li/b/a/b/a/b/a/b/a/b/a/b/a/b/a/b/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/ins/div/div/div/div/img/div/div/h3/div/div/iframe/div/div/a/a/a/a/a/a/a/a/a/a/a/a/img/span/img/span/img/span/img/span/img/span/img/span/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/span/span/span/span/span/span/span/span/span/span/span/span/a/span/span/span/span/span/span/span/span/span/span/a/div/p/a/div/p/a/div/p/div/p/div/p/div/p/img/ins/img/img/img/h3/ul/br/h3/ul/div/b/b/b/b/b/b/b/b/b/b/img/div/div/img/a/span/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/a/img/span/span/span/span/a/img/span/span/span/a/img/span/span/span/a/a/a/a/a/a/a/a/a/a/iframe/li/li/li/li/li/li/h3/ul/div/a/a/a/a/a/a/a/a/a/a/ul/ul/a/img/img/img/a/a/div/a/a/div/a/a/div/a/a/a/a/a/a/a/li/li/a/li/li/li/li/li/li/li/li/li/li/li/img/b/img/b/img/b/img/img/img/a/div/br/a/div/br/img/span/span/span/span/span/span/span/span/span/span/a/span/span/span/span/span/span/span/span/a/img/a/img/a/a/span/span/span/a/span/span/span/a/span/span/a/span/span/span/a/span/span/span/a/span/span/span/a/span/span/span/a/span/span/a/span/span/span/img/a/a/img/a/a/img/a/a/img/a/a/img/a/a/img/a/a/img/a/a/img/a/a/img/a/a";

		System.out.println(TokenBasedDistance.similiarity(a, b, "/"));
	}

}
