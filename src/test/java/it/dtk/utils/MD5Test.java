package it.dtk.utils;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * User: Michele Damiano Torelli
 * Date: 15/07/13
 * Time: 12:16
 */
public class MD5Test {
    private static final int ELEMENTS = 3000000;

//    @Test
//    public void testGetHash() throws Exception {
//        // "MD5 Test" -> "48a491f89c683690789717a910709287"
//        String text = "MD5 Test";
//        String expectedMd5Hash = "48a491f89c683690789717a910709287";
//
//        String md5Hash = MD5.getInstance().getHash(text);
//
//        System.out.println();
//        System.out.println("*** MD5 Test ***");
//        System.out.println("Text: " + text);
//        System.out.println("Expected MD5 hash: " + expectedMd5Hash);
//        System.out.println("Obtained MD5 hash: " + md5Hash);
//
//        assertEquals(expectedMd5Hash, md5Hash);
//    }
//
//    @Test
//    public void testPerformance() throws Exception {
//        final long startTime = UnixEpoch.getTimeStamp();
//
//        for (int i = 0; i < ELEMENTS; i++) {
//            MD5.getInstance().getHash(String.valueOf(i));
//        }
//
//        final long stopTime = UnixEpoch.getTimeStamp();
//
//        System.out.println();
//        System.out.println("*** MD5 Performance Test ***");
//        System.out.println("String array size: " + ELEMENTS);
//        System.out.println("Unix epoch start: " + startTime);
//        System.out.println("Unix epoch stop: " + stopTime);
//        System.out.println("Difference (secs): " + (stopTime - startTime));
//    }
//
//    @Test
//    public void testCollisions() throws Exception {
//        HashMap<String, Boolean> hashes = new HashMap<>(ELEMENTS);
//        int collisions = 0;
//
//        for (int i = 0; i < ELEMENTS; i++) {
//            String hash = MD5.getInstance().getHash(String.valueOf(i));
//
//            if (hashes.get(hash) != null)
//                collisions++;
//            else
//                hashes.put(hash, true);
//        }
//
//        assertEquals(0, collisions);
//
//        System.out.println();
//        System.out.println("*** MD5 Collisions Test ***");
//        System.out.println("String array size: " + ELEMENTS);
//        System.out.println("Collisions: " + collisions);
//    }
}
